import React, { Component } from 'react';
import './App.css';
import Numbers from './Numbers/Numbers'

class App extends Component {

  state = {
    numbersInLoto: [0, 0, 0, 0, 0]
  };

  compareNumbers = (a, b) => {
    return a - b;
  };

  getNewNumbers = () => {
    let array = [];
    let counter = 5;
    while (counter > 0) {
      let random = Math.floor(Math.random() * (37 - 5)) + 5;
      if (array.indexOf(random) === -1) {
        array.push(random);
        counter--;
      }
    }
    array.sort(this.compareNumbers);
    this.setState({numbersInLoto: array});
  };

    render() {
    return (
      <div>
        <div className="App-container">
          <div className="title">Результат последнего тиража</div>
        </div>
        <div className='App-container'>
          {this.state.numbersInLoto.map((number, i) => <Numbers key={i} number={number} />)}
        </div>
        <div className="App-container">
          <button className='btn' onClick={this.getNewNumbers}>Новый розыгрыш</button>
        </div>
      </div>
    );
  }
}

export default App;
